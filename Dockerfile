FROM fedora:36
RUN echo "fastestmirror=true" >> /etc/dnf/dnf.conf && \
    echo "install_weak_deps=false" >> /etc/dnf/dnf.conf && \
    rm /etc/yum.repos.d/fedora-cisco-openh264.repo && \
    rm /etc/yum.repos.d/fedora-modular.repo && \
    rm /etc/yum.repos.d/fedora-updates-modular.repo && \
    rm /etc/yum.repos.d/fedora-updates-testing.repo && \
    rm /etc/yum.repos.d/fedora-updates-testing-modular.repo

RUN dnf -y upgrade && \
      dnf -y install buildah podman runc && \
      dnf clean all
